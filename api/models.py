from django.db import models

class Kategori(models.Model):
    nama_produk = models.TextField()
    
    def _str_(self):
        return self.nama_produk

class Produk(models.Model):
    kategori = models.ForeignKey(Kategori, related_name="produk", on_delete=models.CASCADE)
    nama = models.CharField(max_length=100)
    jenis = models.TextField()
    ukuran = models.TextField()
    warna = models.CharField(max_length=100)
    stok= models.CharField(max_length=100)
    harga = models.CharField(max_length=100)
    def _str_(self):
        return self.kategori

class Penjualan(models.Model):
    produk = models.TextField()
    jumlah_terjual = models.CharField(max_length=100)
    total_harga = models.CharField(max_length=100)
    
    def _str_(self):
        return self.produk


