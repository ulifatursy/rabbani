from rest_framework import serializers
from .models import Kategori, Produk,Penjualan


class KategoriSerializer(serializers.ModelSerializer):
    # produk = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    class Meta:
        model = Kategori
        fields = ["id", "nama_produk"]

class ProdukSerializer(serializers.ModelSerializer):
    class Meta:
        model = Produk
        fields = ["id","kategori","nama","jenis","ukuran","warna","stok","harga"]

class PenjualanSerializer(serializers.ModelSerializer):
    class Meta:
        model = Penjualan
        fields = ["id", "produk","jumlah_terjual","total_harga"]
