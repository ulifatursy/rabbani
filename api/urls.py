from django.urls import path
from . import views

urlpatterns = [
     path("kategori/", views.KategoriListCreate.as_view(), name="kategori-view-create"),
     path("kategori/<int:pk>/",views.KategoriRetrieveUpdateDestroy.as_view(), name="update"),
     path("produk/", views.ProdukListCreate.as_view(), name="produk-view-create"),
     path("produk/<int:pk>/",views.ProdukRetrieveUpdateDestroy.as_view(), name="update"),
     path("penjualan/", views.PenjualanListCreate.as_view(), name="penjualan-view-create"),
     path("penjualan/<int:pk>/",views.PenjualanRetrieveUpdateDestroy.as_view(), name="update"),
]