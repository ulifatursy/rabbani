from django.shortcuts import render
from rest_framework import generics,status
from rest_framework.response import Response
from.models import Kategori, Produk,Penjualan
from.serializers import KategoriSerializer,ProdukSerializer,PenjualanSerializer

class KategoriListCreate(generics.ListCreateAPIView):
    queryset = Kategori.objects.all()
    serializer_class = KategoriSerializer
    def delete(self, request, *args, **kwargs):
        Kategori.objects.all().delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class KategoriRetrieveUpdateDestroy(generics.RetrieveUpdateDestroyAPIView):
    queryset = Kategori.objects.all()
    serializer_class = KategoriSerializer
    lookup_field ="pk"

class ProdukListCreate(generics.ListCreateAPIView):
    queryset = Produk.objects.all()
    serializer_class = ProdukSerializer
    def delete(self, request, *args, **kwargs):
        Produk.objects.all().delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class ProdukRetrieveUpdateDestroy(generics.RetrieveUpdateDestroyAPIView):
    queryset = Produk.objects.all()
    serializer_class = ProdukSerializer
    lookup_field ="pk"

class PenjualanListCreate(generics.ListCreateAPIView):
    queryset = Penjualan.objects.all()
    serializer_class = PenjualanSerializer
    def delete(self, request, *args, **kwargs):
        Penjualan.objects.all().delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class PenjualanRetrieveUpdateDestroy(generics.RetrieveUpdateDestroyAPIView):
    queryset = Penjualan.objects.all()
    serializer_class = PenjualanSerializer
    lookup_field ="pk"


